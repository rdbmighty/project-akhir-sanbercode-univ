<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/jawaban-saya', function () {
    return view('welcome');
});

Route::get('/kategori', function () {
    return view('kategori');
});

//CRUD Kategori
Route::get('/kategori/create', 'KategoriController@create');
Route::post('/kategori', 'KategoriController@store');
Route::get('/kategori', 'KategoriController@index');
Route::get('/kategori/{kategori_id}', 'KategoriController@show');
Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
Route::put('/kategori/{kategori_id}', 'KategoriController@update');
Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');

// Update Profile
Route::get('/profile', "ProfileController@index");
Route::post('/profile', "ProfileController@store");
Route::get('/profile/edit/{profile_id}', 'ProfileController@edit');
Route::put('/profile', 'ProfileController@update');

//CRUD Pertanyaan
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');

//Home
Route::get('/forum/detail/{id}', 'PertanyaanController@detail');
Route::post('/komentar/submit','PertanyaanController@submitkomentar');
Route::delete('/komentar/hapus','PertanyaanController@hapus');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
