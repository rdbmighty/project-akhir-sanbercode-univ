@extends('layouts.master')

@section('title')
   List Pertanyaan
@endsection

@section('content')

    <a href="/pertanyaan/create" class="btn btn-success btn-sm my-2">Tambah</a>

        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Pertanyaan</th>
                <th scope="col">Gambar</th>
                <th scope="col">Kategori</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($pertanyaan as $item)
                    <tr>
                        <td>{{$item->kalimat}}</td>
                        <td>{{$item->gambar}}</td>
                        <td>{{$item->kategori_id}}</td>
                        <td> 
                            <form action="/pertanyaan/{{$item->id}}" method="POST"> 
                                @csrf
                                @method('delete')
                                <a href="/pertanyaan/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                                <a href="/pertanyaan/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>Tidak ada data</td>
                    </tr>
                    
                @endforelse
            </tbody>
        </table>
@endsection