@extends('layouts.master')

@section('content')
<div class="card mb-3">
    <div class="card-body">
      <h5 class="card-title"><b>by: {{$pertanyaan->username}}</b></h5>
      <p class="card-text">{{$pertanyaan->kalimat}}</p>
      <p class="card-text"><small class="text-muted">Kategori: {{$pertanyaan->name}}</small></p>
    </div>
    <button type="button" class="btn btn-primary" style="width: 150px" data-toggle="modal" data-target="#exampleModal">
        Jawabanmu
      </button>
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Isi Jawabanmu</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="/komentar/submit" method="POST">
                @csrf
                <textarea class="form-control" id="exampleFormControlTextarea1" name="tulisan" placeholder="Masukkan Jawabanmu" rows="3"></textarea>
                <input type="hidden" class="form-control" name="pertanyaan_id" value="{{$pertanyaan->id}}" id="title">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            </div>
          </div>
        </div>
      </div>
  </div>
  @foreach ($komentar as $item)
  <div class="card bg-light mb-3 w-50">
    <div class="card-header">{{$item->name}}</div>
    <div class="card-body">
      <h5 class="card-title">{{$item->tulisan}}</h5>
      @if ($item->user_id==Auth::user()->id)
      <form action="/komentar/hapus" method="POST">
        @csrf
        @method('delete')
        <input type="hidden" class="form-control" name="pertanyaan_id" value="{{$pertanyaan->id}}" id="title">
        <input type="hidden" class="form-control" name="komentar_id" value="{{$item->id}}" id="title">
      <button type="submit" class="btn btn-danger">Delete</button>
      </form>
      @endif
    </div>
    
  </div>
  @endforeach
  
  
@endsection
