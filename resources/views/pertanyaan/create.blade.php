@extends('layouts.master')

@section('title')
    Pertanyaan
@endsection

@section('content')
    <form action="/pertanyaan" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>Pertanyaan</label>
            {{-- <label for="title">Nama</label> --}}
            <textarea class="form-control" id="exampleFormControlTextarea1" name="kalimat" placeholder="Masukkan Pertanyaan" rows="3"></textarea>
            @error('kalimat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <label>Gambar</label>
        <div>
            <input type="file" name="gambar">
            @error('gambar')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
          </div>
          <label>Kategori</label>
          <div class="form-group">
            <select class="form-control" name="kategori_id" id="exampleFormControlSelect2" placeholder="Masukkan Kategori">
              @foreach ($kategori as $item)
              <option value="{{$item->id}}">{{$item->name}}</option> 
              @endforeach
            </select>
            @error('kategori_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
          </div>

        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>

@endsection