@extends('layouts.master')

@section('title')
   Detail Kategori
@endsection

@section('content')
<div class="card bg-primary mb-3" style="max-width: 18rem;">
    <div class="card-header text-black">{{$pertanyaan->name}}</div>
    <div class="card-body text-white">
      <h5 class="card-title">{{$pertanyaan->kalimat}}</h5>
      <p class="card-text">{{$pertanyaan->gambar}}</p>
    </div>
@endsection