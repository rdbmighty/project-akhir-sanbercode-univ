@extends('layouts.master')

@section('title')
    Edit Kategori {{$kategori->name}}
@endsection

@section('content')
    <form action="/kategori/{{$kategori->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            {{-- <label for="title">Nama</label> --}}
            <input type="text" value="{{$kategori->name}}" class="form-control" name="nama" id="title" placeholder="Masukkan Nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        
        <button type="submit" class="btn btn-success">Update</button>
    </form>

@endsection