@extends('layouts.master')

@section('title')
    Kategori
@endsection

@section('content')
    <form action="/kategori" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>Kategori</label>
            {{-- <label for="title">Nama</label> --}}
            <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Kategori">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>

@endsection