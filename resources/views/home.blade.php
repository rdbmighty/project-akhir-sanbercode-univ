@extends('layouts.master')

@section('content')
<div class="d-flex flex-row justify-content-around flex-wrap flext-start">
    @foreach ($forum as $item)
    <div class="card m-3" style="width: 18rem;">
        {{-- <img class="card-img-top" src="" > --}}
        <div class="card-body">
          <h5 class="card-title"><b>{{$item->name}}</b></h5>
          <p class="card-text">{{$item->kalimat}}</p>
          <a href="/forum/detail/{{$item->id}}" class="btn btn-primary">Detail</a>
        </div>
    </div>
        @endforeach
</div>


@endsection
