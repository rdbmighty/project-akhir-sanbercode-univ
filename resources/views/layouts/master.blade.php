<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Forum Tanya Jawab Terkait Universitas</title>

    <!-- Custom fonts for this template-->

    @stack('styles')
    <link href="{{asset('tanyajawab/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{asset('tanyajawab/css/sb-admin-2.min.css')}}" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">
    @include('layouts.partials.sidebar')
        <!-- Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
        @include('layouts.partials.nav')
            <!-- Main Content -->
            <div id="content">

                {{-- <div class="content-wrapper">
                    <!-- Content Header (Page header) --> --}}
                    <section class="content-header">
                      <div class="container-fluid">
                        <div class="row mb-2">
                          <div class="col-sm-6">
                            <h1>@yield('title')</h1>
                          </div>
                        </div>
                      </div><!-- /.container-fluid -->
                    </section>

            <section class="content">

                <div class="container-fluid">

                        <!-- Default box -->
                        <div class="card">
                          <div class="card-header">
                  
                            <div class="card-tools">
                            </div>
                          </div>
                          <div class="card-body">
                            @yield('content')
                          </div>
                          <!-- /.card-footer-->
                        </div>
                        <!-- /.card -->
                  
                      </section>

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
   </div>
    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; Website Forum Tanya Jawab Seputar Universitas - 2022</span>
            </div>
        </div>
    </footer> 

    <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
    <!-- End of Footer -->

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('tanyajawab/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('tanyajawab/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('tanyajawab/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('tanyajawab/js/sb-admin-2.min.js')}}"></script>

    @stack('scripts')


</body>

</html>