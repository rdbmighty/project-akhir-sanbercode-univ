@extends('layouts.master')

@section('title')
   Profile
@endsection

@section('content')
{{-- <div class="card" style="width: 18rem;">
    <ul class="list-group list-group-flush">
      <li class="list-group-item">{{$profile->name}}</li>
      <li class="list-group-item">{{$profile->umur}}</li>
      <li class="list-group-item">{{$profile->bio}}</li>
      <li class="list-group-item">{{$profile->alamat}}</li>
    </ul>
  </div> --}}
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Email</th>
                <th scope="col">Umur</th>
                <th scope="col">Biodata</th>                
                <th scope="col">Alamat</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
                    <tr>
                        <td>{{$profile->email}}</td>
                        <td>{{$profile->umur}}</td>
                        <td>{{$profile->bio}}</td>
                        <td>{{$profile->alamat}}</td>
                        <td> <a href="/profile/edit/{{$profile->id}}" class="btn btn-success btn-sm">Edit</a>
                        </td>
                    </tr>
            </tbody>
        </table>
@endsection