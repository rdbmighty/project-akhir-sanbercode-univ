@extends('layouts.master')

@section('title')
    Edit Profile {{$profile->name}}
@endsection

@section('content')
    <form action="/profile" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Email</label>
            <input type="text" value="{{$profile->email}}" class="form-control" name="email" id="title" placeholder="Masukkan Email">
            @error('email')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Umur</label>
            <input type="text" value="{{$profile->umur}}" class="form-control" name="umur" id="title" placeholder="Masukkan Umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Bio</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" name="bio"  rows="3">{{$profile->bio}}</textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Alamat</label>
            <input type="text" value="{{$profile->alamat}}" class="form-control" name="alamat" id="title" placeholder="Masukkan Alamat">
            @error('alamat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        
        <button type="submit" class="btn btn-success">Update</button>
    </form>

@endsection