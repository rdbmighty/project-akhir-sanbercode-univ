<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    public $table = "pertanyaan";

    protected $fillable = [
        'kalimat', 'gambar', 'kategori_id','user_id'
       ];
}
