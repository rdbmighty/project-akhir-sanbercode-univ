<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    public $table = "komentar";

    protected $fillable = [
        'tulisan', 'pertanyaan_id','user_id'
       ];
}
