<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class KategoriController extends Controller
{
    public function create()
    {
        return view('kategori.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ],
        [
            'nama.required' => 'Nama kategori tidak boleh kosong'
            
        ]);
        DB::table('kategori')->insert(
            [
                'name' => $request['nama']
    
            ]
        );
        return redirect('/kategori');
        
    }

    public function index()
    {
        $kategori = DB::table('kategori')->get();

        return view('kategori.index', compact('kategori'));
    }

    public function show($id)
    {
        $kategori = DB::table('kategori')->where('id', $id)->first();
        return view('kategori.show', compact('kategori'));
    }

    public function edit($id)
    {
        $kategori = DB::table('kategori')->where('id', $id)->first();
        return view('kategori.edit', compact('kategori'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
    ],
    [
        'nama.required' => 'nama tidak boleh kosong',
    ]);
            DB::table('kategori')
                ->where('id', $id)
                ->update(
                    [
                        'name' => $request['nama'],
                    ]
                );
            return redirect('/kategori');
    }

    public function destroy($id)
    {
        DB::table('kategori')->where('id', '=', $id)->delete();

        return redirect('/kategori');
    }
}
