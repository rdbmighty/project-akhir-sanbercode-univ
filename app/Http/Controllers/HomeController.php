<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pertanyaan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $forum = Pertanyaan::selectRaw('pertanyaan.id,pertanyaan.kalimat,pertanyaan.kategori_id,kategori.name,pertanyaan.gambar')->leftJoin('kategori','kategori.id','=','pertanyaan.kategori_id')->get();
        $data =[
            'forum'=> $forum
        ];
        return view('home',$data);
    }

}
