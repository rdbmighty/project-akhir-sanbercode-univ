<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pertanyaan;
use App\Komentar;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pertanyaan = Pertanyaan::all()->where('user_id','=', Auth::user()->id);
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = DB::table('kategori')->get();
        $data =[
            'kategori'=> $kategori
        ];
        return view('pertanyaan.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
if($request->gambar){
        $gambar = $request->gambar;
        $encoded_image = explode(".", $gambar)[1];
        $decoded_image = base64_decode($encoded_image);
        $filename = Str::random(30).".png";
	    file_put_contents("gambarpertanyaan/".$filename, $decoded_image);

        Pertanyaan::create([
    		'kalimat' => $request->kalimat,
    		'gambar' => $filename,
            'kategori_id' => $request->kategori_id,
            'user_id' => Auth::user()->id
    	]);
 }else{
Pertanyaan::create([
    		'kalimat' => $request->kalimat,
            'kategori_id' => $request->kategori_id,
            'user_id' => Auth::user()->id
    	]);
}
    	return redirect('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $pertanyaan = Pertanyaan::find($id);
        $pertanyaan = Pertanyaan::find($id)->leftJoin('kategori','kategori.id','=','pertanyaan.kategori_id')->first();
        $data =[
            'pertanyaan'=> $pertanyaan,
        ];
        return view('/pertanyaan.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $kategori = DB::table('kategori')->get();
        $data =[
            'pertanyaan'=> $pertanyaan,
            'kategori' => $kategori
        ];
        return view('/pertanyaan.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'kalimat' => 'required',
            'kategori_id' => 'required'
        ]);
if($request->gambar){
        $gambar = $request->gambar;
        $encoded_image = explode(".", $gambar)[1];
        $decoded_image = base64_decode($encoded_image);
        $filename = Str::random(30).".png";
	    file_put_contents("gambarpertanyaan/".$filename, $decoded_image);

          $pertanyaan = Pertanyaan::find($id);
        $pertanyaan->kalimat = $request->kalimat;
        $pertanyaan->gambar = $filename;
        $pertanyaan->kategori_id = $request->kategori_id;
        $pertanyaan->update();
 }else{
  $pertanyaan = Pertanyaan::find($id);
        $pertanyaan->kalimat = $request->kalimat;
    $pertanyaan->kategori_id = $request->kategori_id;
        $pertanyaan->update();
    	
    }
        return redirect('/pertanyaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $pertanyaan->delete();
        return redirect('/pertanyaan');
    }

    public function detail($id){
        $pertanyaan = Pertanyaan::selectRaw('pertanyaan.id,pertanyaan.kalimat,pertanyaan.kategori_id,kategori.name,pertanyaan.gambar,users.name as username')->leftJoin('kategori','kategori.id','=','pertanyaan.kategori_id')->leftJoin('users','users.id','=','pertanyaan.user_id')->where('pertanyaan.id',$id)->first();
        $komentar = Komentar::selectRaw('komentar.id,komentar.tulisan,komentar.pertanyaan_id, komentar.user_id,users.name')->leftJoin('users','users.id','=','komentar.user_id')->where('pertanyaan_id',$id)->get();
        $data =[
            'pertanyaan'=> $pertanyaan,
            'komentar'=>$komentar
        ];
        return view('pertanyaan.detail',$data);
    }

    public function submitkomentar(Request $request)
    {
        Komentar::create([
    		'tulisan' => $request->tulisan,
            'pertanyaan_id' => $request->pertanyaan_id,
            'user_id' => Auth::user()->id
    	]);
    	return redirect('forum/detail/'.$request->pertanyaan_id);
    }

    public function hapus(Request $request){
        $pertanyaan = Komentar::find($request->komentar_id);
        $pertanyaan->delete();
        return redirect('/forum/detail/'.$request->pertanyaan_id);
    }
}
